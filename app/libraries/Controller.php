<?php
/*

*/

class Controller{

    public function model($model){
        //Require model file
        require_once '../app/models/'.$model.'.php';

        //instantiate model
        return new $model();
    }

    //load view

    public function view($view, $data =[]){
        //check for view file
        if(file_exists('../app/views/'.$view.'.php')){
            require_once '../app/views/'.$view.'.php';
        }else{
            //View doesn't exist
            die('View doesnt exist');
        }

    }
}